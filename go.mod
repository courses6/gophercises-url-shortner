module gitlab.com/courses6/gophercises-url-shortner

go 1.14

require (
	github.com/stretchr/testify v1.5.1
	gopkg.in/yaml.v2 v2.2.2
)
