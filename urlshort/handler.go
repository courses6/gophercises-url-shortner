package urlshort

import (
	"errors"
	"log"
	"net/http"

	"gopkg.in/yaml.v2"
)

var (
	// InvalidYamlFormatErrorMsg ...
	InvalidYamlFormatErrorMsg = "Invalid YAML format"
	// UnmarshalYAMLErrorMsg ...
	UnmarshalYAMLErrorMsg = "Couldn't unmarshal yaml file content"
	// PathNotFoundErrorMsg ...
	PathNotFoundErrorMsg = "Path not found in yml file"
)

type pathToURLStruct struct {
	Path string `yaml:"path"`
	URL  string `yaml:"url"`
}

// MapHandler will return an http.HandlerFunc (which also
// implements http.Handler) that will attempt to map any
// paths (keys in the map) to their corresponding URL (values
// that each key in the map points to, in string format).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path
		if url, ok := pathsToUrls[path]; ok {
			http.Redirect(w, r, url, http.StatusPermanentRedirect)
			return
		}
		fallback.ServeHTTP(w, r)
	}
}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc (which also implements http.Handler)
// that will attempt to map any paths to their corresponding
// URL. If the path is not provided in the YAML, then the
// fallback http.Handler will be called instead.
//
// YAML is expected to be in the format:
//
//     - path: /some-path
//       url: https://www.some-url.com/demo
//
// The only errors that can be returned all related to having
// invalid YAML data.
//
// See MapHandler to create a similar http.HandlerFunc via
// a mapping of paths to urls.
func YAMLHandler(yml []byte, fallback http.Handler) (http.HandlerFunc, error) {
	if yml == nil {
		return nil, errors.New(InvalidYamlFormatErrorMsg)
	}
	var pathsToUrlsStruct []pathToURLStruct
	err := yaml.Unmarshal(yml, &pathsToUrlsStruct)
	if err != nil {
		log.Println(err.Error())
		return nil, errors.New(UnmarshalYAMLErrorMsg)
	}
	pathsToUrls := make(map[string]string)
	for _, pu := range pathsToUrlsStruct {
		pathsToUrls[pu.Path] = pu.URL
	}
	return MapHandler(pathsToUrls, fallback), nil
}
