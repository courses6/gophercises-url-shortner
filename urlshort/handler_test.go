package urlshort

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMapHandlerPathNotProvided(t *testing.T) {
	// initialize
	called := false
	fallback := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		called = true
	})
	r := httptest.NewRequest("GET", "/foo", nil)
	w := httptest.NewRecorder()

	// execute
	MapHandler(
		map[string]string{
			"/test": "/test-url-short",
		},
		fallback,
	)(w, r)

	// assert
	assert.True(t, called)
}

func TestMapHandlerPathFound(t *testing.T) {
	// initialize
	fallback := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	r := httptest.NewRequest("GET", "/test", nil)
	w := httptest.NewRecorder()

	// execute
	handler := http.HandlerFunc(MapHandler(
		map[string]string{
			"/test": "https://test-url.com",
		},
		fallback,
	))
	handler.ServeHTTP(w, r)

	// assert
	assert.EqualValues(t, http.StatusPermanentRedirect, w.Code)
	location, err := w.Result().Location()
	assert.Nil(t, err)
	assert.Contains(t, `<a href="https://test-url.com">Permanent Redirect</a>.`, strings.TrimSpace(w.Body.String()))
	assert.EqualValues(t, "test-url.com", location.Host)
}

func TestYAMLHandlerNilYAMLData(t *testing.T) {
	fallback := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	_, err := YAMLHandler(nil, fallback)
	assert.NotNil(t, err)
	assert.EqualValues(t, InvalidYamlFormatErrorMsg, err.Error())
}

func TestYAMLHandlerInvalidYAMLData(t *testing.T) {
	mapHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	yaml := `wrong-yaml-file-format`
	_, err := YAMLHandler([]byte(yaml), mapHandler)
	assert.NotNil(t, err)
	assert.EqualValues(t, UnmarshalYAMLErrorMsg, err.Error())
}

func TestYAMLHandlerValidYAMLData(t *testing.T) {
	mapHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	yaml := `
- path: /test
  url: https://test-url.com
`
	handler, err := YAMLHandler([]byte(yaml), mapHandler)
	assert.Nil(t, err)
	assert.NotNil(t, handler)
}

func TestYAMLHandlerPathFound(t *testing.T) {
	// initialize
	yaml := `
- path: /test
  url: https://test-url.com
- path: /second-test
  url: https://second-test-url.com
`
	mapHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	r := httptest.NewRequest("GET", "/second-test", nil)
	w := httptest.NewRecorder()

	// execute
	yamlHandler, _ := YAMLHandler(
		[]byte(yaml),
		mapHandler,
	)
	httpHandler := http.HandlerFunc(yamlHandler)
	httpHandler.ServeHTTP(w, r)

	// assert
	assert.EqualValues(t, http.StatusPermanentRedirect, w.Code)
	location, err := w.Result().Location()
	assert.Nil(t, err)
	assert.Contains(t, `<a href="https://second-test-url.com">Permanent Redirect</a>.`, strings.TrimSpace(w.Body.String()))
	assert.EqualValues(t, "second-test-url.com", location.Host)
}
